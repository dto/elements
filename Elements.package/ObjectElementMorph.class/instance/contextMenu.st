as yet unclassified
contextMenu
	|m|
	m := CustomMenu new.
	(owner class == PaletteElementMorph)
		ifTrue: [m add: 'remove' action: #removeFromPalette ]
		ifFalse: [
		 	self isRenameable ifTrue: [
				m add: 'rename...' action: #editLabel ]].
	m addLine.
	m add: 'show code...' action: #showGeneratedSmalltalk.

	^m