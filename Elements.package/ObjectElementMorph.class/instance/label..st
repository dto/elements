as yet unclassified
label: aString

	super label: aString.

	(owner isKindOf: PaletteElementMorph)
		ifTrue: [^self color: owner ownerColor twiceDarker ].

	(#('self' 'super' 'nil' 'true' 'false') includes: aString)
		ifTrue: [^self color: Color red darker].

	aString first isUppercase
		ifTrue: [
			(Smalltalk classNames includes: aString asSymbol)
				ifTrue: [^self color: Color magenta darker darker].
			(Smalltalk keys includes: aString asSymbol)
				ifTrue: [^self color: Color magenta twiceDarker darker].

			^self color: Color blue twiceDarker ].

	self color: Color blue darker..

