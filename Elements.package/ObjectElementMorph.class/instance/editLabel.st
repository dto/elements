as yet unclassified
editLabel
	|ans|

	ans := FillInTheBlank request: 'rename:' initialAnswer: label contents.
	(ans size > 0) ifTrue: [self label: ans]