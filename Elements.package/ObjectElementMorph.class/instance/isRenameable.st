as yet unclassified
isRenameable
	| editor method |
	editor := (self ownerThatIsA: ElementsMethodEditorMorph).
	editor isNil ifTrue: [^true].
	method := (self ownerThatIsA: MethodElementMorph).
	method ifNotNil: [
		^(self ownerThatIsA: StepHolderElementMorph) isNil].
	^false
