as yet unclassified
arrangePanes

	| toggleWidth |
	toggleWidth := self width - 6 // 2.

	instance 
		width: toggleWidth;
		position: self topLeft + 2;
		fitMorphs.

	class 
		width: toggleWidth;
		position: instance right + 2 @ (instance top);
		fitMorphs.

	self height: class bottom + 2 - self top