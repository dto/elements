as yet unclassified
buildPanes

	instance := ElementsToggleButtonMorph new
				target: self;
				selector: #instance;
				label: 'instance';
				onColor: Color blue darker offColor: color.

	class := ElementsToggleButtonMorph new
				target: self;
				selector: #class;
				label: 'class';
				onColor: Color magenta twiceDarker offColor: color.

	self
		addMorph: instance;
		addMorph: class
