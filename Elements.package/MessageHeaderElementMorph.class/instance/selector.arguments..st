as yet unclassified
selector: aString arguments: anArray

	"for decompilation purposes only. Caution: this method assumes that the number of arguments matches the
	number of parameters required by the selector."
	| tokens lb arg |

	selector := aString.

	label ifNotNil: [label delete. label := nil].
	labels do: [:each| each delete ].
	labels := OrderedCollection new.
	arguments := anArray.
	variables do: [:each| each delete ].
	variables := OrderedCollection new.

	tokens := selector findTokens: ':'.
	1 to: tokens size do: [:i |
		lb := ((StringMorph 
				contents: (self wordsFrom: (tokens at: i)) 
				font: self labelFont) 
				color: self labelColor).
		labels add: lb.
		self addMorph: lb.
		(anArray size < i) ifFalse: [
			arg := ObjectElementMorph new label: (anArray at: i) asString.
			arg color: owner color twiceDarker.
			variables add: arg.
			self addMorph: arg]].

	self fit
