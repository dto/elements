as yet unclassified
contextMenu
	|m|
	m := CustomMenu new.
	m add: 'rename...' action: #editSelector.
	arguments isEmpty ifTrue: [
		m add: 'add argument' action: #beBinary ].
	^m