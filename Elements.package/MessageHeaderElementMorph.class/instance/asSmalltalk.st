as yet unclassified
asSmalltalk
	| kw ws |
	kw := selector keywords.
	ws := WriteStream on: String new.
	1 to: kw size do: [:i|
		ws nextPutString: (kw at: i).
		(variables size < i) ifFalse: [
			ws nextPutString: ' ', (variables at: i) asSmalltalk, ' ' ]].
	^ws contents