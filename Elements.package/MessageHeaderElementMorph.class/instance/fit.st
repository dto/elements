as yet unclassified
fit
	| cw bw bw2 x arg lb maxHeight |

	labels ifNil: [^self].
	cw := (self labelFont widthOf: $ ) // 2.
	bw := self borderWidth.
	bw2 := bw * 2.

	x := self left + bw.
	maxHeight := self labelFont height.

	1 to: labels size do: [:i |
		lb := labels at: i.
		lb position: x @ self center y.
		x := lb right + cw.
		(variables size >= i)
			ifTrue: [
				arg := variables at: i.
				maxHeight := maxHeight max: arg height.
				arg position: x @ self center y.
				x := arg right + cw]].

	self extent: (x - self position x - cw - bw2) @ maxHeight + (bw2 * 2).
	variables do: [:each|
		each position: (each position x @ (bounds center y - (each height // 2))) ].
	labels do: [:each|
		each position: (each position x @ (bounds center y - (each height // 2))) ].

	(owner respondsTo: #fit)
		ifTrue: [^owner fit]