as yet unclassified
editSelector
	|ans parms tokens|

	parms := OrderedCollection new.
	ans := FillInTheBlank request: 'selector:' initialAnswer: selector.
	ans isEmpty ifTrue: [^self].
	ans := (SyntaxElementMorph selectorFrom: ans) asString.
	(ans includes: ($:)) ifFalse: [
		^self selector: ans arguments: #() ].

	tokens := ans findTokens: ':'.
	1 to: tokens size do: [:i |
		parms add: 'arg', i printString ].

	self selector: ans arguments: parms.