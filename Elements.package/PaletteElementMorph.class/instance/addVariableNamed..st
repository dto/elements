as yet unclassified
addVariableNamed: aSymbol

	| v |
	v := ObjectElementMorph new label: aSymbol asString.
	v color: self varColor.
	variables at: aSymbol put: v.
	self addMorph: v.
	self fit