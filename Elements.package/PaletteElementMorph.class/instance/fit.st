as yet unclassified
fit
	| x y bw2|
	bw2 := self borderWidth * 2.

	variables isEmpty ifTrue: [
		self extent: 0@0.
		(owner respondsTo: #fit)
			ifTrue: [^owner fit]
			ifFalse: [^self]].

	self height: variables anyOne height + (bw2 * 2).
	x := self left + bw2.
	y := (self center - (variables anyOne extent // 2)) y.
	variables keys asSortedCollection do: [:vn |
		(variables at: vn) position: x @ y.
		x := (variables at: vn) right + bw2.
		].
	self width: (x - self position x).

	(owner respondsTo: #fit)
		ifTrue: [^owner fit]