as yet unclassified
fit

	| cw bw y|
	label ifNil: [^self].
	bw := self borderWidth.
	cw := self labelFont widthOf: $ .

	self width: expression width + label width + cw + (bw * 3).
	self height: expression height + (bw * 7).


	label position: (self left + bw) @ (bounds center - (label extent // 2)) y.

	expression position: (label right + cw) @ (bounds center - (expression extent // 2)) y.

	(owner respondsTo: #fit)
		ifTrue: [owner fit]