as yet unclassified
asSmalltalk

	contents ifNil: [^'nil '].
	((#(	BlockElementMorph
		LiteralElementMorph
		ObjectElementMorph
	) includes: contents class printString asSymbol)
		or: [owner isKindOf: StepElementMorph])
		ifTrue: [^contents asSmalltalk].

	(contents isKindOf: MessageElementMorph) ifTrue: [
		contents isUnary ifTrue: [^contents asSmalltalk].

		contents isBinary ifTrue: [
			owner isKeywords ifTrue: [ ^contents asSmalltalk]]

	].

	^'(', contents asSmalltalk, ')'