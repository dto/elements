as yet unclassified
fit
	| h |
	contents isNil
		ifTrue: [
			h := MessageElementMorph labelFont height.
			self extent: h @ h]
		ifFalse: [
			self extent: contents extent " + (self borderWidth * 2)".
			"(owner isKindOf: StepElementMorph)
				ifTrue: [self extent: self extent + (self borderWidth * 2)]."
			contents position: bounds center - (contents extent // 2) ].
	(owner respondsTo: #fit)
		ifTrue: [owner fit]