as yet unclassified
rootForGrabOf: aMorph
	"Answer the root of the morph structure to be picked up when the given morph is grabbed."

	owner ifNotNil: [^owner rootForGrabOf: aMorph].
	^nil

"	(self isSticky and: [self isPartsDonor not])
		ifTrue: [^ nil]
		ifFalse: [
			(owner isNil or: [owner isWorldOrHandMorph])
				ifTrue: [^ self]
				ifFalse: [
					owner allowSubmorphExtraction
						ifTrue: [^ self]
						ifFalse: [^ owner rootForGrabOf: aMorph]]].
"