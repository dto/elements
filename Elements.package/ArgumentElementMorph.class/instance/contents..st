as yet unclassified
contents: anElement
	contents ifNotNil: [contents delete].
	anElement representsNil ifFalse: [
		contents := anElement.
		self addMorphFront: contents ].
	self fit.
	self unHilite
