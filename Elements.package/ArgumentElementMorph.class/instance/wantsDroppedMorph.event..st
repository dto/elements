as yet unclassified
wantsDroppedMorph: aMorph event: evt

	^contents isNil 
		& (self ownerThatIsA: ElementPaletteMorph) isNil
		& ((aMorph isKindOf: ObjectElementMorph) 
		| (aMorph isKindOf: LiteralElementMorph) 
		| (aMorph isKindOf: BlockElementMorph) 
		| (aMorph isKindOf: MessageElementMorph))