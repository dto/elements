as yet unclassified
fitMorphs
	| bw x y |
	chooser ifNil: [^super fitMorphs].
	bw := self borderWidth.
	self height: label height + (bw * 2).
	y := self top + bw.

	chooser extent: (label height @ label height) * 2//3.
	chooser position: self left + bw @ self top + (label height - chooser height // 2).

	x := chooser right + bw + (self width - label width // 2).

	label position: x@ y.
