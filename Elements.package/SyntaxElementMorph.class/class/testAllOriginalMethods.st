as yet unclassified
testAllOriginalMethods

	| eachClass methods original compiled text node|
	Smalltalk classNames do: [: className |
		eachClass := Smalltalk classNamed: className.
		methods := eachClass methodDict.
		methods keysDo: [ :eachSelector|
			original := methods at: eachSelector.
			text := ((eachClass decompilerClass new)
				decompile: eachSelector in: eachClass method: original) printString.
			node := Compiler new 
				compile: text 
				in: eachClass 
				notifying: nil 
				ifFail: [self inform: 'compilation failed'].
			compiled := node generate: #(0 0 0 0).
			original = compiled
				ifTrue: [Transcript show:'.']
				ifFalse: [Transcript cr; 
					show: className asString, ' -> ', 
						eachSelector printString; cr]]]
		
		