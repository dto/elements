as yet unclassified
wordsFrom: camelCase
	| ws cap |
	
	cap := camelCase first isUppercase.

	(camelCase = 'ifTrue')
		ifTrue: [^'yes'].
	(camelCase = 'ifFalse')
		ifTrue: [^'no '].
	(camelCase = ':=')
		ifTrue: [^':='].

	ws := WriteStream on: String new.

	camelCase do: [:c|
		c isUppercase 
			ifTrue: [ 
			ws nextPut: $ ; 
			nextPut: (cap ifTrue: [c] ifFalse: [c asLowercase ])]
			ifFalse: [ 
				ws nextPut: c.
				(c = ($:)) ifTrue: [ws nextPut: $ ]]].
	^ws contents
