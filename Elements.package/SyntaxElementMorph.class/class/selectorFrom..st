as yet unclassified
selectorFrom: aUserString
	"answer a camel-cased Symbol beginning with a lowercase character"

	|ans ws char|
	ans := self classNameFrom: aUserString.
	ans at: 1 put: (ans at: 1) asLowercase.
	ws := WriteStream on: String new.
	1 to: ans size do: [:i|
		char := ans at: i.
		(char isUppercase and: [ (ans at: i - 1) = ($:)])
			ifTrue: [ws nextPut: char asLowercase]
			ifFalse: [ws nextPut: char]].
	^ws contents asSymbol