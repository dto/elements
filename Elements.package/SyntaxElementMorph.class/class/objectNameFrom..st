as yet unclassified
objectNameFrom: aUserString
	"answer a camel-cased String beginning with a lowercase character"

	|ans|
	ans := self classNameFrom: aUserString.
	ans at: 1 put: (ans at: 1) asLowercase.
	^ans