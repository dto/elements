as yet unclassified
isDecompilable: selector class: class

	|  original compiled text node|
	original := class methodDict at: selector.
	text := ((class decompilerClass new)
		decompile: selector in: class method: original) printString.
	node := Compiler new 
		compile: text 
		in: class 
		notifying: nil 
		ifFail: [self inform: 'compilation failed'].
	compiled := node generate: #(0 0 0 0).
	original = compiled
		ifFalse: [Transcript nextPut: $-].
	^original = compiled
		
		