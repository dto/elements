as yet unclassified
classNameFrom: aUserString
	"answer a camel-cased uppercased String"

	^(aUserString allWordsCapitalized reject: [:c|
		c = $ ]) capitalized