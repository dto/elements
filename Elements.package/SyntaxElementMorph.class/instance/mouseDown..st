as yet unclassified
mouseDown: evt
	"Handle a mouse down event."

	evt whichButtonString = 'blue '
		ifTrue: [ ^self invokeContextMenu].
	self startDrag: evt
