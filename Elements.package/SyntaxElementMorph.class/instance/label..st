as yet unclassified
label: aString

	name := aString.
	label ifNil: [
		label := StringMorph contents: '' font: self labelFont.
		label color: self labelColor.
		self addMorphFront: label].
	label contents: (self wordsFrom: name).
	self fit
