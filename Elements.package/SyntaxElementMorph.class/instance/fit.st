as yet unclassified
fit
	self extent: label extent + (self borderWidth * 2).
	self width: (self width + (self labelFont widthOf: $ )).
	label position: bounds center - (label extent // 2).
	(owner respondsTo: #fit)
		ifTrue: [owner fit]
