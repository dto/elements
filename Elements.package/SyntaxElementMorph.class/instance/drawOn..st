as yet unclassified
drawOn: aCanvas 

	|r bw |
	bw := self borderWidth.

	"border"
	"top"
	r := Rectangle origin: ((self left + bw) @ self top) corner: (self right @ (self top + bw)).
	aCanvas fillRectangle: r color: self borderColorVeryLight.

	"left"
	r := Rectangle origin: (self topLeft) corner: (self left + bw) @ (self bottom - bw).
	aCanvas fillRectangle: r color: self borderColorLight.

	"right"
	r := Rectangle origin: ((self right - bw) @ (self top + bw)) corner: self bottomRight.
	aCanvas fillRectangle: r color: self borderColorDark.

	"bottom"
	r := Rectangle origin: (self left @ (self bottom - bw)) corner: (self right - bw) @ self bottom.
	aCanvas fillRectangle: r color: self borderColorVeryDark.

	"body"
"	aCanvas fillRectangle: (Rectangle origin: (self topLeft + bw) corner: (self bottomRight - bw)) color: color"

	aCanvas fillRectangle: (Rectangle origin: (self topLeft + bw) corner: (self right - bw) @ (self center y - bw)) color: color twiceLighter.

	aCanvas fillRectangle: (Rectangle origin: (self left + bw) @ (self center y - bw) corner: (self right - bw) @ (self center y)) color: color lighter.

	aCanvas fillRectangle: (Rectangle origin: (self left + bw @ (self center y)) corner: (self bottomRight - bw)) color: color

