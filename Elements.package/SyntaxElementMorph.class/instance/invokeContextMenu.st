as yet unclassified
invokeContextMenu
	|m choice|
	m := self contextMenu.
	m ifNotNil: [
		choice := m startUp.
		choice ifNotNil: [self perform: choice] ]