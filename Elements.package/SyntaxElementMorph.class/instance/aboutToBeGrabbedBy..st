as yet unclassified
aboutToBeGrabbedBy: aHand

	(owner isKindOf: ArgumentElementMorph)
		ifTrue: [owner removeContents. ^self ].
	(owner isKindOf: StepElementMorph)
		ifTrue: [owner removeNext. ^self].
	(owner isKindOf: MessageElementMorph)
		ifTrue: [owner removeCascadedElement: self. ^self].
	^ self


