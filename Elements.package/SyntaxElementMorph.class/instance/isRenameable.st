as yet unclassified
isRenameable
	| editor |
	editor := (self ownerThatIsA: ElementsMethodEditorMorph).
	^editor isNil
	