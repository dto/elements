as yet unclassified
wantsDroppedMorph: aMorph event: evt

	^contents isNil & (aMorph isKindOf: StepElementMorph) & (self ownerThatIsA: ElementPaletteMorph) isNil

