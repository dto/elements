as yet unclassified
fit
	| h |

	(owner isKindOf: MethodElementMorph) & (self ownerThatIsA: ElementsMethodEditorMorph) notNil
		ifTrue: [ ^self fitMethod ].

	contents isNil
		ifTrue: [
			h := MessageElementMorph labelFont height.
			self extent: (h * 1.8) @ (h * 1.2)]
		ifFalse: [
			(owner isKindOf: MethodElementMorph).
			self extent: contents stackExtent  + (self borderWidth * 2).
			contents position: bounds center - (contents stackExtent // 2) ].
	(owner respondsTo: #fit)
		ifTrue: [owner fit]