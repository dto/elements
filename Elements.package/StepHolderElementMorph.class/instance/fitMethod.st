as yet unclassified
fitMethod
	| h |
	h := MessageElementMorph labelFont height.
	self extent: (h * 1.8) @ (h * 1.2).
	contents ifNotNil: [
		contents position: self position + self borderWidth].
	(owner respondsTo: #fit)
		ifTrue: [owner fit]