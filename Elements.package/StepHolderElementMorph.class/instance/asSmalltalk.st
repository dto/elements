as yet unclassified
asSmalltalk
	|stack ws|
	contents ifNil: [^''].
	stack := contents stack.
	ws := WriteStream on: String new.
	stack do: [:eachStep|
		ws nextPutString: eachStep asSmalltalk.
		(eachStep == stack last)
			ifFalse: [ws nextPut: $.;cr]].
	^ws contents