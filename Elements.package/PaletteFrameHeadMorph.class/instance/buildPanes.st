as yet unclassified
buildPanes

	basics := ElementsToggleButtonMorph new
				target: self;
				selector: #basics;
				label: 'Basics';
				onColor: Color green twiceDarker darker offColor: color.

	variables := ElementsDoubleToggleMorph new
				target: self;
				selector: #variables;
				chooser: #varMenu;
				label: 'Variables';
				onColor: Color blue darker offColor: color.

	classes := ElementsDoubleToggleMorph new
				target: self;
				selector: #classes;
				chooser: #classMenu;
				label: 'Classes';
				onColor: Color magenta darker darker darker offColor: color.

	messages := ElementsDoubleToggleMorph new
				target: self;
				selector: #messages;
				chooser: #chooseClassForMethods;
				label: 'Messages';
				onColor: Color orange darker darker offColor: color.

	self
		addMorph: basics;
		addMorph: variables;
		addMorph: classes;
		addMorph: messages
