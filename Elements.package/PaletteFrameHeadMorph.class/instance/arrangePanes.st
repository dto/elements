as yet unclassified
arrangePanes

	| toggleWidth |
	toggleWidth := self width - 6 // 2.

	basics 
		width: toggleWidth;
		position: self topLeft + 2;
		fitMorphs.

	messages 
		width: toggleWidth;
		position: basics left @ (basics bottom + 2);
		fitMorphs.

	variables 
		width: toggleWidth;
		position: (basics right + 2) @ (basics top);
		fitMorphs.

	classes 
		width: toggleWidth;
		position: (basics right + 2) @ (variables bottom + 2);
		fitMorphs.

	self height: messages bottom + 2 - self top