as yet unclassified
initialize
	super initialize.
	choice := #basics.
	lastMsgChoice := #currentMessages.
	color := Color gray.
	self width: 180.
	self buildPanes.
	self arrangePanes.
	self refresh