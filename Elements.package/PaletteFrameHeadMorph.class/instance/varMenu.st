as yet unclassified
varMenu
	| menu isClassSide |
	menu := MenuMorph new.
	menu defaultTarget: self.
	isClassSide := frame currentClass isKindOf: Metaclass.
	isClassSide ifFalse: [
		menu add: 'instance' action: #instanceVars ].
	menu add: 'class' action: #classVars.
	menu add: 'globals' action: #globalVars.
	menu popUpNearHand