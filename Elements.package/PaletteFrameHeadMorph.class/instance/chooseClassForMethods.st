as yet unclassified
chooseClassForMethods

	| menu packages pkg pkgMenu catMenu superc subc smenu currentClass |

	self messages.	

	packages := Dictionary new.
	SystemOrganization categories asSortedCollection do: [:cat |
		pkg := (cat asString findTokens: #( $- )) first.
		(packages includesKey: pkg)
			ifFalse: [ packages at: pkg put: OrderedCollection new].
		(packages at: pkg) add: cat].

	currentClass := self currentMethodsClass.

	superc := currentClass allSuperclasses.
	subc := currentClass allSubclasses.

	menu := MenuMorph new.

	menu defaultTarget: self.
	menu add: 'current class' action: #currentMessages.

	currentClass isMeta
		ifTrue: [ menu add: 'instance side' selector: #setMethodsClass: argument: currentClass theNonMetaClass]
		ifFalse: [ menu add: 'class side' selector: #setMethodsClass: argument: currentClass class].

	superc isEmpty ifFalse: [
		smenu := MenuMorph new defaultTarget: self.
		superc do: [:eachClass |
			smenu add: eachClass printString selector: #setMethodsClass: argument: eachClass ].
		menu add: 'superclasses' subMenu: smenu ].

	subc isEmpty ifFalse: [
		smenu := MenuMorph new defaultTarget: self.
		subc do: [:eachClass |
			smenu add: eachClass printString selector: #setMethodsClass: argument: eachClass ].
		menu add: 'subclasses' subMenu: smenu].

	menu addLine.

	packages keys asSortedCollection do: [:eachPkg |
		pkgMenu := MenuMorph new.
		(packages at: eachPkg) asSortedCollection do: [: cat |
			catMenu := self menuForCategory: cat.
			pkgMenu add: (SyntaxElementMorph wordsFrom: cat asString)
					subMenu: catMenu ].
		menu add: eachPkg subMenu: pkgMenu ].

	menu popUpNearHand