as yet unclassified
refresh
	basics off.
	classes off.
	messages off.
	variables off.

	(#basics = choice) ifTrue: [
		basics on ].
	(#(allClasses currentPackageClasses) includes: choice) ifTrue: [
		classes on ].
	(#(variables instanceVars classVars globalVars) includes: choice) ifTrue: [
		variables on ].
	(#(currentMessages methodsClass) includes: choice) ifTrue: [
		messages on ].

	frame ifNotNil: [frame refresh]