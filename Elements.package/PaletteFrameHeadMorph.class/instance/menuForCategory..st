as yet unclassified
menuForCategory: categoryName

	|menu list submenu |
	menu := MenuMorph new
				defaultTarget: self.
	list := SystemOrganization listAtCategoryNamed: categoryName.
	list asSortedCollection do: [:eachClass|
		submenu := MenuMorph new defaultTarget: self.
		submenu add: 'instance' selector: #setMethodsClass: argument: (Smalltalk classNamed: eachClass).
		submenu add: 'class' selector: #setMethodsClass: argument: (Smalltalk classNamed: eachClass) class.
		menu add: (SyntaxElementMorph wordsFrom: eachClass asString) subMenu: submenu].
	^menu
