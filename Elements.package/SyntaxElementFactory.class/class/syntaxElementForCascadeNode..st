as yet unclassified
syntaxElementForCascadeNode: aCascadeNode
	| me |
	me := aCascadeNode messages last asSyntaxElement
		receiver: aCascadeNode receiver asSyntaxElement.
	aCascadeNode messages allButLast do: [:msg |
		me addToCascade: msg asSyntaxElement ].

	^me