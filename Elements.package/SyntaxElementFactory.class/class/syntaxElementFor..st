as yet unclassified
syntaxElementFor: anObject 
	anObject class == AssignmentNode
		ifTrue: [^ self syntaxElementForAssignmentNode: anObject].
	anObject class == BlockNode
		ifTrue: [^ self syntaxElementForBlockNode: anObject].
	anObject class == CascadeNode
		ifTrue: [^ self syntaxElementForCascadeNode: anObject].
	anObject class == LiteralNode
		ifTrue: [^ self syntaxElementForLiteralNode: anObject].
	anObject class == MessageNode
		ifTrue: [^ self syntaxElementForMessageNode: anObject].
	anObject class == MethodNode
		ifTrue: [^ self syntaxElementForMethodNode: anObject].
	anObject class == ParseNode
		ifTrue: [^ self syntaxElementForParseNode: anObject].
	anObject class == ReturnNode
		ifTrue: [^ self syntaxElementForReturnNode: anObject].
	anObject class == VariableNode
		ifTrue: [^ self syntaxElementForVariableNode: anObject].
	self error: 'cannot find syntax element for this object'.