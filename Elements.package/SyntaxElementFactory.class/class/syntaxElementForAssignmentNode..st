as yet unclassified
syntaxElementForAssignmentNode: anAssignmentNode
	^MessageElementMorph new
		receiver: anAssignmentNode variable asSyntaxElement;
		selector: #':=' arguments: (Array with: (self syntaxElementFor: (anAssignmentNode value))).