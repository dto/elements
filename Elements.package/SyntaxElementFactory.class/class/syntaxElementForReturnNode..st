as yet unclassified
syntaxElementForReturnNode: aReturnNode

^ReturnElementMorph new expression: (self syntaxElementFor: aReturnNode expr).