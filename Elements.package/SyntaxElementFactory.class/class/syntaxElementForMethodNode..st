as yet unclassified
syntaxElementForMethodNode: aMethodNode 
	| element pe be |
	element := MethodElementMorph new
				selector: aMethodNode selector asString
				arguments: (aMethodNode arguments
						collect: [:arg | arg key]).
	aMethodNode temporaries
		do: [:temp | element addVariableNamed: temp key].
	aMethodNode primitive > 0
		ifTrue: [(aMethodNode primitive between: 255 and: 519)
				ifFalse: [pe := self primitiveElementFor: aMethodNode.
					element addStep: pe]].
	be := self findStackOfSteps: aMethodNode block.
	be
		ifNotNil: [element addStep: be].
	^ element