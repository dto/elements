as yet unclassified
syntaxElementForBlockNode: aBlockNode 
	| be thisStatement step first |
	be := BlockElementMorph new.
	be
		addVarNames: (aBlockNode arguments
				collect: [:arg | arg key]).
	1
		to: aBlockNode statements size
		do: [:i | 
			thisStatement := aBlockNode statements at: i.
			(thisStatement isKindOf: ReturnNode)
				ifTrue: [step := thisStatement asSyntaxElement]
				ifFalse: [step := StepElementMorph new expression: (self syntaxElementFor: thisStatement)].
			i = 1
				ifTrue: [first := step]
				ifFalse: [first addToLast: step]].
	first
		ifNotNil: [be addStep: first].
	^ be