as yet unclassified
findStackOfSteps: aBlockNode 
	| thisStatement step first |
	first := nil.
	1
		to: aBlockNode statements size
		do: [:i | 
			thisStatement := aBlockNode statements at: i.
			(thisStatement isKindOf: ReturnNode)
				ifTrue: [thisStatement isReturnSelf
						ifTrue: [^ first].
					step := thisStatement asSyntaxElement]
				ifFalse: [step := StepElementMorph new
								expression: (self syntaxElementFor: thisStatement)].
			i = 1
				ifTrue: [first := step]
				ifFalse: [first addToLast: step]].
	^ first