as yet unclassified
syntaxElementForMessageNode: aMessageNode
	|me|
	me := MessageElementMorph new.
	aMessageNode receiver ifNotNil: [
		me receiver: (self syntaxElementFor: aMessageNode receiver) ].
	me 
		selector: aMessageNode selector key "self selectorForSyntaxElement"
		arguments: (aMessageNode arguments collect: [:arg| 
			arg asSyntaxElement ]).
	^me