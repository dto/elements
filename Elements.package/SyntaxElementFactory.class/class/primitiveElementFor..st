as yet unclassified
primitiveElementFor: aMethodNode 
	| s primIndex primDecl |
	s := WriteStream on: String new.
	primIndex := aMethodNode primitive.
	primIndex = 120
		ifTrue: ["External call spec"
			1 halt.
			^ s print: aMethodNode encoder literals first].
	s nextPutAll: 'primitive: '.
	primIndex = 117
		ifTrue: [primDecl := aMethodNode encoder literals at: 1.
			s nextPut: $';
				
				nextPutAll: (primDecl at: 2);
				 nextPut: $'.
			(primDecl at: 1) notNil
				ifTrue: [s nextPutAll: ' module:';
						 nextPut: $';
						
						nextPutAll: (primDecl at: 1);
						 nextPut: $']]
		ifFalse: [s print: primIndex].
	^ PrimitiveElementMorph new contents: s contents