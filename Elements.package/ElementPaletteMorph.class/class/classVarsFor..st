as yet unclassified
classVarsFor: aClass
	| pal |
	pal := self new.
	aClass allClassVarNames asSortedCollection do: [:vname|
		pal addElement: (ObjectElementMorph new label: vname asString) ].
	^pal
