as yet unclassified
globals
	| pal |
	pal := self new.
	(Smalltalk keys reject: [:key| Smalltalk classNames includes: key]) asSortedCollection do: [:global|
		pal addElement: (ObjectElementMorph new label: global asString) ].
	^pal
