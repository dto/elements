as yet unclassified
basics
	^self new
		addElement: StepElementMorph new;
		addElement: ReturnElementMorph new;
		addElement: PrimitiveElementMorph new;
		addElement: BlockElementMorph new;
		addElement: (ObjectElementMorph new label: 'self');
		addElement: (ObjectElementMorph new label: 'super');
		addElement: (ObjectElementMorph new label: 'true');
		addElement: (ObjectElementMorph new label: 'false');
		addElement: (LiteralElementMorph new);
		addElement: MessageElementMorph new assign;
		addElement: MessageElementMorph new yesNo
