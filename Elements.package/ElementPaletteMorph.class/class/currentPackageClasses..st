as yet unclassified
currentPackageClasses: aClass
	| pal packages pkg |
	pkg := ((SystemOrganization categoryOfElement: aClass name asSymbol) asString findTokens: #( $- )) first.
	packages := self packageClassesDict.
	pal := self new.
	(packages at: pkg) asSortedCollection do: [:class|
		pal addElement: (ObjectElementMorph new label: class asString) ].
	^pal
