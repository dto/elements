as yet unclassified
classes
	| pal |
	pal := self new.
	Smalltalk classNames asSortedCollection do: [:class|
		pal addElement: (ObjectElementMorph new label: class asString) ].
	^pal
