as yet unclassified
messagesFor: aClass
	
	| pal me|
	pal := self new.
	pal addElement: ((StringMorph contents: (SyntaxElementMorph wordsFrom: aClass name asString) font: self labelFont) color: self labelColor).
	aClass selectors asSortedCollection do: [:selector|
		me := MessageElementMorph new selector: selector.
		((selector asString includes: ($:)) not and: [
			(aClass lookupSelector: selector) numArgs > 0])
				ifTrue: [me beBinary].

		pal addElement: me ].
	^pal
