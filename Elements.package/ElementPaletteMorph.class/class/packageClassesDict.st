as yet unclassified
packageClassesDict
	| packages pkg ans |
	packages := Dictionary new.
	ans := Dictionary new.
	SystemOrganization categories asSortedCollection do: [:cat |
		pkg := (cat asString findTokens: #( $- )) first.
		(packages includesKey: pkg) ifFalse: [
				packages at: pkg put: OrderedCollection new.
				ans at: pkg put: OrderedCollection new].
		(packages at: pkg) add: cat].

	packages keys do: [:eachPkg |
		(packages at: eachPkg) do: [: cat |
			(ans at: eachPkg) addAll:  (SystemOrganization listAtCategoryNamed: cat) ]].
	^ans