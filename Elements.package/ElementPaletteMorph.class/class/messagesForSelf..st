as yet unclassified
messagesForSelf: aClass
	
	| pal me|
	pal := self new.
	aClass allSelectors asSortedCollection do: [:selector|
		me := MessageElementMorph new selector: selector.
		((selector asString includes: ($:)) not and: [
			(aClass lookupSelector: selector) numArgs > 0])
				ifTrue: [me beBinary].
		me receiver: (ObjectElementMorph new label: 'self').
		pal addElement: me ].
	^pal
