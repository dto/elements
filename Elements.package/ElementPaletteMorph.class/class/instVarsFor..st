as yet unclassified
instVarsFor: aClass
	| pal vs |
	pal := self new.
	vs := aClass allInstVarNames asSortedCollection.
	vs do: [:vname|
		pal addElement: (ObjectElementMorph new label: vname asString) ].
	^pal
