as yet unclassified
addElement: anElement
	|maxWidth|
	self addMorph: anElement.
	elements isEmpty
		ifTrue:[ anElement position: self position + (2@2)]
		ifFalse: [ anElement position: self position + (2@(elements last bottom + 2)) ].
	elements add: anElement.
	self height: (elements last bottom + 2) - (self top).
	maxWidth := elements last width + 4.
	elements do: [:element|
		maxWidth := (element width + 4) max: maxWidth ].
	self width: maxWidth