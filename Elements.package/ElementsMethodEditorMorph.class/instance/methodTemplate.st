as yet unclassified
methodTemplate
	| m |
	m := MethodElementMorph new selector: '' arguments: #().
	self currentMethodElement delete.
	m position: editor contents position.
	editor contents addMorph: m fitMethod.
	self isInWorld ifFalse: [^self].
	methodChooser selector: nil.
	self methodUnmodified