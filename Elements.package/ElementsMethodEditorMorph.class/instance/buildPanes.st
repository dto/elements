as yet unclassified
buildPanes
	| p m |
	classChooser := ClassChooserMorph new frame: self; class: Object.
	sideChooser := InstanceClassSwitchMorph new frame: self.
	methodChooser := MethodChooserMorph new frame: self; class: Object; selector: #yourself.
	editor := ElementsScrollFrameMorph new
				color: Color gray;
				borderColor: Color gray.
	p := PasteUpMorph new borderWidth: 0; color: Color gray darker.
	m := MethodElementMorph new selector: '' arguments: #().
	editor contents: p.
	p addMorph: m.

	palette := ElementsPaletteFrameMorph new editorFrame: self; height: self height; arrangePanes.
	saver := ElementsToggleButtonMorph new flat; label: 'save'; onColor: color offColor: color; target: self; selector: #saveCurrentMethod.
	self
		addMorph: classChooser;
		addMorph: sideChooser;
		addMorph: methodChooser;
		addMorph: editor;
		addMorph: palette;
		addMorph: saver.
	saver hide.