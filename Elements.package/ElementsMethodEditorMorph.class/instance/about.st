as yet unclassified
about
	self inform: '- Elements -

a graphical Smalltalk
------------------------------
experimental release of Feb. 24 2009
 
written by Jens Mönig (jens@moenig.org)
all rights reserved
 
inspired by Scratch from the MIT Media Lab
and based in part on the Scratch Source Code.
Implemented in Squeak and in itself.'