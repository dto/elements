as yet unclassified
contextMenu
	|m|
	m := CustomMenu new.
	m add: 'clean up' action: #arrangeElements.
	m addLine.
	fillScreenOn
		ifTrue: [
			m add: 'switch to development mode' action: #developmentMode.
]
		ifFalse: [m add: 'switch to user mode' action: #userMode.
			m add: 'save image in user mode' action: #saveImage ].
	m addLine.
	m add: 'about...' action: #about.

	^m