as yet unclassified
developmentMode
	fillScreenOn := false.
	self
		position: World topLeft;
		extent: World extent - 50;
		arrangePanes.
	palette arrangePanes.	
	Preferences disable: #noviceMode.
	Preferences enable: #warnIfNoSourcesFile.
	Preferences enable: #warnIfNoChangesFile.

