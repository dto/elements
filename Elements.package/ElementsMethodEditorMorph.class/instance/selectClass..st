as yet unclassified
selectClass: aClass
	editor ifNil: [^self].
	(currentClass = aClass)
		ifTrue: [^self].

	currentClass := aClass.
	classChooser class: currentClass.
	methodChooser class: currentClass.
	palette class: currentClass.
	self methodTemplate
	