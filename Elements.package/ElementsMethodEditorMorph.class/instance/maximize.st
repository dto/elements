as yet unclassified
maximize
	((self position = World position) and: [
		self extent = World extent])
		ifTrue: [^self].
	self
		position: World topLeft;
		extent: World extent;
		arrangePanes.
	palette arrangePanes.
	self comeToFront
