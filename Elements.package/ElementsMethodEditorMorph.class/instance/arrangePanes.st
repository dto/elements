as yet unclassified
arrangePanes
	classChooser position: self topLeft + 2.
	sideChooser position: classChooser bottomLeft.
	methodChooser position: sideChooser bottomLeft.
	editor position: (self left + 4) @ (methodChooser bottom + 4);
		extent: (self width - 180 - 8) @ (self bottom - editor top - 4).
	palette position: editor right @ (classChooser bottom);
		height: self bottom - palette top - 4;
		arrangePanes.
	saver position: (self position x + (palette left - methodChooser right - saver width // 2) ) @ (methodChooser bottom - saver height)
