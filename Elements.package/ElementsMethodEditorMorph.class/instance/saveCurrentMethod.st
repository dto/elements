as yet unclassified
saveCurrentMethod
	| se sel|
	se := self currentMethodElement.
	sel := se selector.

	currentClass compile: se asSmalltalk
					classified: ClassOrganizer nullCategory
					notifying: nil.

	methodChooser selector: sel.
	self selectMethod: sel.
	palette refresh