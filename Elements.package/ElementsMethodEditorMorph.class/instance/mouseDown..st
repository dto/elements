as yet unclassified
mouseDown: evt
	"Handle a mouse down event."

	evt whichButton = 1
		ifTrue: [ ^self invokeContextMenu].
	Preferences noviceMode ifTrue: [^self].
	self startDrag: evt
