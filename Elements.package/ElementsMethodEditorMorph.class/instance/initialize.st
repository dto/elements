as yet unclassified
initialize
	fillScreenOn := false.
	methodModified := false.
	currentClass := Object.
	currentMethod := #yourself.
	super initialize.
	self extent: 600 @ 400.
	color := Color gray.
	self buildPanes.
	self arrangePanes.
	self refresh