as yet unclassified
selectMethod: aSelector
	| myMethod temps se |

	currentMethod := aSelector.
	editor ifNil: [^self].
	currentMethod ifNil: [^self].
	currentClass ifNotNil:
		[myMethod := currentClass compiledMethodAt: currentMethod ifAbsent: [^self methodTemplate].
		temps := (currentClass compilerClass new
						parse: myMethod getSourceFromFile asString in: currentClass notifying: nil)
						tempNames.

	se := self findSyntaxElementFor: ((currentClass decompilerClass new withTempNames: temps)
		decompile: currentMethod in: currentClass method: myMethod).

	self currentMethodElement delete.
	se position: editor contents position.
	editor contents addMorph: se fitMethod.

	^self methodUnmodified.
		].

	^self methodTemplate

