as yet unclassified
arrangeElements
	| current x y spacing |
	spacing := 5.
	current := self currentMethodElement.
	x := editor contents left + spacing.
	y := current stackBottom + spacing.
	editor contents submorphsDo: [:element |
		(element == current) ifFalse:[
			element position: x @ y.
			y := element bottom + spacing ]]
		