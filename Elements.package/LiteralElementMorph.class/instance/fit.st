as yet unclassified
fit

	| bw cw handle |
	bw := self borderWidth.
	cw := self labelFont widthOf: $ .
	handle := cw * 2.

	self extent: textBox extent + (bw * 4).
	self width: self width + handle.

	textBox position: (self left + handle + bw) @ (self center - (textBox extent // 2)) y.

	(owner respondsTo: #fit)
		ifTrue: [owner fit]
