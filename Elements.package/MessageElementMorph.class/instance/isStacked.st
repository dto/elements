as yet unclassified
isStacked
	^((#(
		'isNil:'
		'ifNil:'
		'ifNotNil:'
		'ifTrue:'
		'ifFalse:'
		'ifTrue:ifFalse:'
		'ifFalse:ifTrue:'
		'select:'
		'detect:'
		'detect:ifNone:'
		'collect:'
		'reject:'
		'select:'
		'do:'
		'to:do:'
		'whileTrue:'
		'or:'
		'and:'
	)
	includes: selector) or: [
	(arguments select: [:arg | 
		arg contents isKindOf: BlockElementMorph])
			size > 0 ]) or: [ selector notNil and: [
				(selector findTokens: ':') size > 3] ]
