as yet unclassified
isLabelFirst
	|firstWord disabled|
	disabled := true. "change this to experiment with swapped unaries"
	disabled ifTrue: [^false].

	selector ifNil: [^false].
	firstWord := (self label findTokens: ' ') first. 
		(#('is' 'as' 'has'	'wants') includes: firstWord)
			ifTrue: [ ^false].

	^arguments isEmpty & cascade isEmpty & receiver isHidden not