as yet unclassified
removeCascadedElement: element

	cascade remove: element ifAbsent: [].
	self fit.
	element extractFromCascade.
	^self