as yet unclassified
fitStacked

	| h w cw bw bw2 x y arg lb maxRight |
	cw := self labelFont widthOf: $ .
	bw := self borderWidth.
	bw2 := bw * 2.

	receiver position: (self left + bw2) @ (self top + bw2).
	x := receiver right + cw.
	y := receiver bottom + bw.

	labels isEmpty 
		ifTrue: [
			h := receiver height + (bw2 * 2).
			w := receiver width + (label width) + (cw * 1.5) + (bw2 * 2).

			self width: w.
			self height: h.

			label position: x @ (bounds center y - (label height // 2)) ]
		ifFalse: [
			maxRight := receiver right.
			1 to: labels size do: [:i |
				arg := arguments at: i.
				lb := labels at: i.

				lb position: receiver left + cw @ (y + ((arg height - lb height) // 2)).
				arg position: (lb right + cw) @ y.

				y := y + arg height + bw.

				maxRight := maxRight max: (arg right) ].

			self extent: ((maxRight - self left) + bw2) @ ((y - self top) + bw)].

	(owner respondsTo: #fit)
		ifTrue: [owner fit]