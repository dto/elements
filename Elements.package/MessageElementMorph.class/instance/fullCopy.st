as yet unclassified
fullCopy
	|copy|
	copy := self class new selector: selector.
	(#ifTrue:ifFalse: = selector) 
		ifTrue: [copy yesNo ].
	(#:= = selector) 
		ifTrue: [copy assign ].
	copy position: self position.
	(copy argumentCount < self argumentCount)
		ifTrue: [copy beBinary].
	^copy