as yet unclassified
buildLabelsAndArgs

	| tokens lb arg |
	label ifNotNil: [label delete. label := nil].
	labels do: [:each| each delete ].
	arguments do: [:each| each delete ].
	labels := OrderedCollection new.
	arguments := OrderedCollection new.

	((selector includes: $:) not | (selector = ':=')) ifTrue: [
		self label: (self wordsFrom: selector).
		^self ].

	tokens := selector findTokens: ':'.
	tokens do: [:each| 
		lb := ((StringMorph contents: (self wordsFrom: each) font: self labelFont) color: self labelColor).
		labels add: lb.
		self addMorph: lb.
		arg := ArgumentElementMorph new.
		arguments add: arg.
		self addMorph: arg].
		