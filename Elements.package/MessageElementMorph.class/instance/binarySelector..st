as yet unclassified
binarySelector: aString

	| lb arg |

	selector := aString.
	label ifNotNil: [label delete. label := nil].
	labels do: [:each| each delete ].
	arguments do: [:each| each delete ].
	labels := OrderedCollection new.
	arguments := OrderedCollection new.

	lb := ((StringMorph contents: aString font: self labelFont) color: self labelColor).
	labels add: lb.
	self addMorph: lb.
	arg := ArgumentElementMorph new.
	arguments add: arg.
	self addMorph: arg.
	self fit