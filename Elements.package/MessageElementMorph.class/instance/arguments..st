as yet unclassified
arguments: anArray

	1 to: anArray size do: [:i|
		(arguments size < i) ifFalse: [
			(arguments at: i) contents: (anArray at: i ) ]]