as yet unclassified
fitForCascade

	| cw bw bw2 x arg lb maxHeight |
	cw := self labelFont widthOf: $ .
	bw := self borderWidth.
	bw2 := bw * 2.

	x := self left + bw2.

	labels isEmpty 
		ifTrue: [^super fit]
		ifFalse: [
			maxHeight := labels anyOne height.
			1 to: labels size do: [:i |
				arg := arguments at: i.
				lb := labels at: i.
				maxHeight := maxHeight max: (arg height).

				lb position: x @ (lb position y).
				x := x + lb width + cw.
				arg position: x @ (arg position y).
				x := x + arg width + cw ].
			self extent: (x - self position x - cw - bw2) @ maxHeight + (bw2 * 2).
			receiver position: (receiver position x @ (bounds center y - (receiver height // 2))).
			arguments do: [:each|
				each position: (each position x @ (bounds center y - (each height // 2))) ].
			labels do: [:each|
				each position: (each position x @ (bounds center y - (each height // 2))) ].
].

	(owner respondsTo: #fit)
		ifTrue: [owner fit]