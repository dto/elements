as yet unclassified
initialize
	super initialize.
	labels := OrderedCollection new.
	arguments := OrderedCollection new.
	cascade := OrderedCollection new.
	color := Color orange darker.
	self addLabel.
	self addReceiver.
	self fit.