as yet unclassified
addLabel
	label := StringMorph contents: '' font: self labelFont.
	label color: self labelColor.
	self addMorphFront: label.
