as yet unclassified
wantsDroppedMorph: aMorph event: evt

	(owner isKindOf: self class) ifTrue: [owner hasCascade ifTrue: [^false]].
	^(aMorph isKindOf: self class) and: [ (aMorph hasCascade not) & (aMorph receiver isNil) ]