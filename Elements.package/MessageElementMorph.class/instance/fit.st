as yet unclassified
fit

	receiver visible ifTrue: [^self fitForCascade ].
	cascade isEmpty not ifTrue: [ ^self fitCascaded ].
	self isStacked
	ifTrue: [self fitStacked]
	ifFalse: [self fitInLine]