as yet unclassified
fitCascaded
	| cw bw bw2 x y arg lb maxHeight maxRight labelTop labelBottom cntr |
	cw := self labelFont widthOf: $ .
	bw := self borderWidth.
	bw2 := bw * 2.

	x := receiver right + bw2.
	maxRight := x.
	y := self top + bw2.

	cascade do: [:eachElement |
		eachElement position: x @ y.
		maxRight := maxRight max: eachElement right.
		y := eachElement bottom ].
	x := x + (bw * 3).
	labelTop := y + bw.

	labels isEmpty 
		ifTrue: [
			label position: x @ labelTop.
			maxRight := maxRight max: label right.
			maxHeight := label height.
			labelBottom := label bottom ]
		ifFalse: [
			maxHeight := labels anyOne height.
			1 to: labels size do: [:i |
				arg := arguments at: i.
				lb := labels at: i.
				maxHeight := maxHeight max: (arg height).

				lb position: x @ labelTop.
				x := x + lb width + cw.
				arg position: x @ labelTop.
				x := x + arg width + cw ].
			x := x - cw.
			maxRight := maxRight max: x.

			cntr := labelTop + (maxHeight // 2).

			arguments do: [:each|
				each position: (each position x @ (cntr - (each height // 2))) ].
			labels do: [:each|
				each position: (each position x @ (cntr - (each height // 2))) ]].

	labelBottom := labelTop + maxHeight.
	labelBottom := labelBottom max: receiver bottom.

	self extent: ((maxRight - self left) @ (labelBottom - self top)) + bw2.



	(owner respondsTo: #fit)
		ifTrue: [owner fit]