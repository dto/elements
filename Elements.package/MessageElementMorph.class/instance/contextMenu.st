as yet unclassified
contextMenu
	|m|
	m := CustomMenu new.
	self isRenameable ifTrue: [
		m add: 'rename...' action: #editSelector.
		arguments isEmpty ifTrue: [
			m add: 'add argument' action: #beBinary ].
		m addLine ].
	m add: 'show code...' action: #showGeneratedSmalltalk.
	m add: 'show result...' action: #showResult.
	m add: 'evaluate' action: #evaluate.
	^m