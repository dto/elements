as yet unclassified
keywordsAndArgs
	| kw ws |
	kw := selector keywords.
	(selector = ':=') ifTrue: [kw := #(':=')].
	ws := WriteStream on: String new.
	1 to: kw size do: [:i|
		ws nextPutString: (kw at: i).
		(arguments size < i) ifFalse: [
			ws nextPutString: ' ', (arguments at: i) asSmalltalk, ' ' ]].
	^ws contents