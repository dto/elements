as yet unclassified
rootForGrabOf: aMorph
	"Answer the root of the morph structure to be picked up when the given morph is grabbed."

	aMorph == self ifTrue: [^super rootForGrabOf: aMorph ].
	(aMorph isKindOf: self class) ifTrue: [^self].
	^super rootForGrabOf: aMorph
