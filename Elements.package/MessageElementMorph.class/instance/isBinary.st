as yet unclassified
isBinary

	^(arguments size = 1) and: [
		selector asString last ~= ($:)]