as yet unclassified
editSelector
	|ans|

	ans := FillInTheBlank request: 'selector:' initialAnswer: selector.
	(ans size > 0) ifTrue: [self selector: ans]