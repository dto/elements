as yet unclassified
asSmalltalk
	| ws |
	receiver isHidden ifTrue: [^self keywordsAndArgs ].
	cascade isEmpty not ifTrue: [ ^self cascadeCode ].

	ws := WriteStream on: String new.
	^ws
		nextPutString: receiver asSmalltalk, ' ';
		nextPutString: self keywordsAndArgs;
		contents