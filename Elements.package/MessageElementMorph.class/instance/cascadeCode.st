as yet unclassified
cascadeCode
	| ws |
	ws := WriteStream on: String new.
	ws nextPutString: receiver asSmalltalk.
	cascade do: [:msg|
		ws cr;
			nextPutString: msg asSmalltalk, ' ';
			nextPut:$; ].
	^ws
		nextPutString: self keywordsAndArgs;
		cr; 
		contents