as yet unclassified
selector: aString arguments: anArray

	"for decompilation purposes only. Caution: this method assumes that the number of arguments matches the
	number of parameters required by the selector."

	| tokens lb arg |

	selector := aString.
	label ifNotNil: [label delete. label := nil].
	labels do: [:each| each delete ].
	arguments do: [:each| each delete ].
	labels := OrderedCollection new.
	arguments := OrderedCollection new.

	anArray isEmpty ifTrue: [
		self label: (self wordsFrom: selector).
		^self fit ].

	tokens := selector findTokens: ':'.
	1 to: tokens size do: [:i |
		lb := ((StringMorph 
				contents: (self wordsFrom: (tokens at: i)) 
				font: self labelFont) 
				color: self labelColor).
		labels add: lb.
		self addMorph: lb.
		arg := ArgumentElementMorph new.
		arguments add: arg.
		self addMorph: arg.
		arg contents: (anArray at: i)].

	self fit