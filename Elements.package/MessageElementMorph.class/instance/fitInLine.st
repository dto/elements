as yet unclassified
fitInLine

	| h w cw bw bw2 x arg lb maxHeight |
	cw := self labelFont widthOf: $ .
	bw := self borderWidth.
	bw2 := bw * 2.

	receiver position: (self left + bw2) @ (self top + bw2).
	x := receiver right + cw.

	labels isEmpty 
		ifTrue: [
			h := receiver height + (bw2 * 2).
			w := receiver width + (label width) + (cw * 1.5) + (bw2 * 2).

			self width: w.
			self height: h.

			label position: x @ (bounds center y - (label height // 2)).

			self isLabelFirst ifTrue: [
				label position: (receiver position x) @ (label position y).
				receiver position: (label right + cw) @ (receiver position y) ]]
		ifFalse: [
			maxHeight := receiver height.
			1 to: labels size do: [:i |
				arg := arguments at: i.
				lb := labels at: i.
				maxHeight := maxHeight max: (arg height).

				lb position: x @ (lb position y).
				x := x + lb width + cw.
				arg position: x @ (arg position y).
				x := x + arg width + cw ].
			self extent: (x - self position x - cw - bw2) @ maxHeight + (bw2 * 2).
			receiver position: (receiver position x @ (bounds center y - (receiver height // 2))).
			arguments do: [:each|
				each position: (each position x @ (bounds center y - (each height // 2))) ].
			labels do: [:each|
				each position: (each position x @ (bounds center y - (each height // 2))) ].
].

	(owner respondsTo: #fit)
		ifTrue: [owner fit]