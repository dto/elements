as yet unclassified
resizeToLabel
	| le h ne |
	label ifNil: [^self ].

	le := (label extent + (self borderWidth * 2) + ((self labelFont widthOf: $ ) @ 0)).
	h := MessageElementMorph labelFont height.
	ne := ( h max: le x ) @ ( h max: le y).
	(self extent = ne) ifFalse: [self extent: ne ].

	(owner respondsTo: #fit)
		ifTrue: [owner fit]
