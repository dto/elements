as yet unclassified
label: aString
	label ifNil: [
		label := StringElementMorph contents: aString font: self labelFont.
		label color: self labelColor.
		self addMorphFront: label ].
	label contents: aString.
	self fit
