as yet unclassified
menuForCategory: categoryName

	|menu list |
	menu := MenuMorph new
				defaultTarget: self.
	list := SystemOrganization listAtCategoryNamed: categoryName.
	list asSortedCollection do: [:each|
		menu add: (self wordsFrom: each asString) selector: #class: argument: (Smalltalk classNamed: each)].
	^menu
