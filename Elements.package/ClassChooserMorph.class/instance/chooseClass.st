as yet unclassified
chooseClass

	|menu list packages pkg pkgMenu catMenu |
	packages := Dictionary new.
	SystemOrganization categories asSortedCollection do: [:cat |
		pkg := (cat asString findTokens: #( $- )) first.
		(packages includesKey: pkg)
			ifFalse: [ packages at: pkg put: OrderedCollection new].
		(packages at: pkg) add: cat].

	menu := MenuMorph new.
	packages keys asSortedCollection do: [:eachPkg |
		pkgMenu := MenuMorph new.
		(packages at: eachPkg) asSortedCollection do: [: cat |
			catMenu := self menuForCategory: cat.
			pkgMenu add: (self wordsFrom: cat asString)
					subMenu: catMenu ].
		"pkgMenu add: (self wordsFrom: eachPkg asString)
				subMenu: pkgMenu."
		menu add: eachPkg subMenu: pkgMenu ].



"
	list := SystemOrganization categories asSortedCollection.
	list do: [:each|
		menu add: (self wordsFrom: each asString) subMenu: (self menuForCategory: each)].
"
	menu popUpAt: button center x @ (self bottom + 5) forHand: World activeHand in: World