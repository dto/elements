as yet unclassified
class: aClass
	| classToSet |
	classToSet := aClass.

	frame ifNotNil: [
		(frame currentSide = #class)
		& (aClass isKindOf: Metaclass) not
			ifTrue: [ classToSet := aClass class ]].

	class = classToSet ifTrue: [^self].
	class := classToSet.
	label ifNil: [
		label := StringMorph contents: '' font: self labelFont.
		label color: self labelColor.
		self addMorphFront: label ].
	label contents: (self wordsFrom: class name asString).
	self fitMorphs.

	frame ifNotNil: [
		frame selectClass: class]