as yet unclassified
fitMorphs
	| bw x y |
	bw := self borderWidth.
	self height: label height + (bw * 2).
	x := self left + ((self width - label width) // 2).
	y := self top + bw.
	label position: x@ y 