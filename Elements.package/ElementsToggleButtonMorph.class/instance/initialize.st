as yet unclassified
initialize
	super initialize.
	specialEffect := true.
	on := true.
	target := self.
	selector := #toggle.
	onColor := Color blue.
	offColor := Color gray.
	self label: 'Elements Toggle Button'.
	self toggle