as yet unclassified
addVariable
	|ans|

	ans := FillInTheBlank request: 'add variable:'.
	(ans size > 0) ifTrue: [self addVarNames: (Array with: (SyntaxElementMorph objectNameFrom: ans))]