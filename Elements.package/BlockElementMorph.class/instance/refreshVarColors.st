as yet unclassified
refreshVarColors

	self allMorphsDo: [:m|
		((m isMemberOf: ObjectElementMorph) and: [
			self allVariableNames includes: m name]) ifTrue: [
				m color: self varColor ]]