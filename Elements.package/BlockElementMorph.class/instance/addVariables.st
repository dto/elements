as yet unclassified
addVariables
	|ans|

	ans := FillInTheBlank request: 'add variables:'.
	(ans size > 0) ifTrue: [self addVarNames: (ans findTokens: ' ')]