as yet unclassified
initialize
	super initialize.
	color := Color paleBlue darker.
	palette := PaletteElementMorph new.
	self addMorph: palette.
	steps := StepHolderElementMorph new.
	steps color: color slightlyDarker.
	self addMorph: steps.
	self fit.