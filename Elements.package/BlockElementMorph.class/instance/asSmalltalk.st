as yet unclassified
asSmalltalk
	| ws vn|
	ws := WriteStream on: String new.
	ws nextPut: $[.
	(vn := self variableNames) isEmpty ifFalse: [
		vn do: [:n|
			ws nextPut: $:.
			ws nextPutString: n, ' ' ].
		ws nextPutString: '| '].
	ws nextPutString: steps asSmalltalk, ']'.
	^ws contents