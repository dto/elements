as yet unclassified
addStep: aStepElement
	steps contents isNil 
		ifTrue: [steps contents: aStepElement]
		ifFalse: [steps contents addToLast: aStepElement]