as yet unclassified
contextMenu
	|m|
	m := CustomMenu new.
	m add: 'add variable...' action: #addVariable.
	m addLine.
	m add: 'show code...' action: #showGeneratedSmalltalk.
	m add: 'show result...' action: #showResult.
	m add: 'evaluate' action: #evaluate.
	^m