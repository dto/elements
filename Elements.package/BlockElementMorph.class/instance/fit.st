as yet unclassified
fit
	| bw bw2 cw handle x y|
	bw := self borderWidth.
	bw2 := bw * 2.
	cw := self labelFont widthOf: $ .
	handle := cw * 2.

	palette position: (self left + handle) @ (self top).
	(palette height = 0)
		ifTrue: [y := self top + (bw * 3)]
		ifFalse: [y := palette bottom].
	

	steps position: (self left + bw2 + handle) @ y.

	x := (steps right + bw2) max: palette right.

	self width: x - self left.
	(palette height = 0)
		ifTrue: [ self height: steps height + (self borderWidth * 5) ]
		ifFalse: [self height: steps height + palette height + (bw2) ].

	self refreshVarColors.

	(owner respondsTo: #fit)
		ifTrue: [owner fit]
