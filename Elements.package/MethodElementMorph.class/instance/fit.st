as yet unclassified
fit
	| bw bw2 cw handle x y editor|

	header ifNil: [^self ].
	bw := self borderWidth.
	bw2 := bw * 2.
	cw := self labelFont widthOf: $ .
	handle := cw.

	header position: (self left + bw + handle) @ (self top).

	palette position: (self left + handle) @ (header bottom).
	(palette height = 0)
		ifTrue: [y := header bottom + (bw * 3)]
		ifFalse: [y := palette bottom].
	

	steps position: (self left + bw2 + handle) @ y.

	x := ((steps right + bw2) max: palette right) max: header right.

	self width: x - self left.
	(palette height = 0)
		ifTrue: [ self height: steps height + header height + (bw * 6) ]
		ifFalse: [self height: steps height + palette height + header height + (bw2) ].

	self refreshVarColors.

	(owner respondsTo: #fit)
		ifTrue: [^owner fit].

	editor := (self ownerThatIsA: ElementsMethodEditorMorph).
	editor ifNotNil: [editor methodModified ]