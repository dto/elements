as yet unclassified
contextMenu
	|m|
	m := super contextMenu.
	self isRenameable ifTrue: [
		m add: 'rename...' action: #editSelector ].
	^m