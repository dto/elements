as yet unclassified
initialize
	super initialize.
	color := Color paleTan.
	steps color: color slightlyDarker.
	header := MessageHeaderElementMorph new.
	self addMorph: header.
	self fit.