as yet unclassified
asSmalltalk
	| ws vn|
	ws := WriteStream on: String new.
	ws nextPutString: header asSmalltalk; cr; cr.
	(vn := self localVariableNames) isEmpty ifFalse: [
		ws nextPutString: '| '.
		vn do: [:n|
			ws nextPutString: n, ' ' ].
		ws
			nextPutString: '|';
			nextPut: Character cr].
	ws nextPutString: steps asSmalltalk.
	^ws contents