as yet unclassified
fitMethod
	|editor|
	(self findA: StepHolderElementMorph) fitMethod.
	editor := (self ownerThatIsA: ElementsMethodEditorMorph).
	editor ifNotNil: [editor methodModified ]