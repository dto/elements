as yet unclassified
asCompiledMethodForClass: aClass
	| mn1 cm st |
	st := self asSmalltalk.
	mn1 := Compiler new compile: st in: aClass notifying: nil ifFail: [self inform: 'compiling failed'].
	cm := mn1 generate: #(0 0 0 0).
	^cm 