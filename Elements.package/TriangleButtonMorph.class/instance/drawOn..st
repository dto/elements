as yet unclassified
drawOn: aCanvas
	aCanvas
		line: self topLeft + 1 to: (self topRight + (-1@1)) color: self borderColorVeryLight;
		line: (self topRight + (-1@1)) to: (self center x @ (self bottom - 1)) color: self borderColorVeryDark;
		line: self center x @ (self bottom - 1) to: self topLeft + 1 color: self borderColorLight
