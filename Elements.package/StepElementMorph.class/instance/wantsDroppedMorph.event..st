as yet unclassified
wantsDroppedMorph: aMorph event: evt

	^next isNil & (aMorph isKindOf: self class) & (self ownerThatIsA: ElementPaletteMorph) isNil
