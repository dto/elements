as yet unclassified
last
	self isLast
		ifTrue: [^self]
		ifFalse: [^next last]