as yet unclassified
fit

	| cw handle bw x y|
	bw := self borderWidth.
	cw := self labelFont widthOf: $ .
	handle := cw * 3.

	self width: expression width  + handle + (bw * 3).
	self height: expression height + (bw * 7).


	x := self left + bw + handle.
	y := (bounds center - (expression extent // 2)) y.

	expression position: x @ y.

	next isNil ifFalse:[
		next position: self left @ (self bottom - (self borderWidth * 2))].

	(owner respondsTo: #fit)
		ifTrue: [owner fit]