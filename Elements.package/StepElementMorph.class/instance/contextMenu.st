as yet unclassified
contextMenu
	|m|
	m := CustomMenu new.
	m add: 'show code...' action: #showGeneratedSmalltalk.
	m add: 'show result...' action: #showResult.
	m add: 'evaluate' action: #evaluate.
	^m