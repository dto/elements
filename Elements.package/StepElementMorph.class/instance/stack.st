as yet unclassified
stack
	| list step |
	list := OrderedCollection with: self.
	step := self.
	[step next notNil] whileTrue: [
		step := step next.
		list add: step ].
	^list
	