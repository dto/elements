as yet unclassified
stackExtent
	| w h |
	w := self width.
	h := 0.
	self stack do: [:each|
		w := w max: each width.
		h := h + each height].
	h := h - ((self borderWidth * 2) * (self stack size -1)).
	^w@h