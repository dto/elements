as yet unclassified
removeNext
	| holder |
	next ifNotNil: [next := nil ].
	holder := self ownerThatIsA: StepHolderElementMorph.
	holder ifNotNil: [holder fit]