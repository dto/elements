as yet unclassified
createClassVar
	|varName current|
	currentClass isMeta
		ifTrue: [current := currentClass theNonMetaClass ]
		ifFalse: [current := currentClass ].

	varName := FillInTheBlank request: 'variable name:' initialAnswer: ''.
	varName isEmpty ifTrue: [^self].
	current addClassVarName: varName.
	self refresh		