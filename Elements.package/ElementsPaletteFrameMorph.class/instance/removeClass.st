as yet unclassified
removeClass
	| class |
	class := self selectClass.
	class ifNotNil: [
		class := Smalltalk at: class.
		self removeClass: class ]