as yet unclassified
createGlobalVar
	|varName |

	varName := FillInTheBlank request: 'variable name:' initialAnswer: ''.
	varName isEmpty ifTrue: [^self].
	varName := SyntaxElementMorph classNameFrom: varName.
	(Smalltalk includesKey: varName asSymbol)
		ifTrue: [^self inform: varName, ' is already in use'].
	Smalltalk at: varName asSymbol put: nil.
	self refresh		