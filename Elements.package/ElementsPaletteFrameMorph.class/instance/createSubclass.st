as yet unclassified
createSubclass
	|className categoryName current newClass|
	currentClass isMeta
		ifTrue: [current := currentClass theNonMetaClass ]
		ifFalse: [current := currentClass ].

	className := FillInTheBlank request: 'class name:' initialAnswer: ''.
	className isEmpty ifTrue: [^self].
	categoryName := FillInTheBlank request: 'category:' initialAnswer: 'UserObjects'.
	categoryName isEmpty ifTrue: [^self].

	className := SyntaxElementMorph classNameFrom: className.

	newClass := current subclass: className asSymbol
		instanceVariableNames: ''
		classVariableNames: ''
		poolDictionaries: ''
		category: categoryName.

	editorFrame selectClass: newClass

		