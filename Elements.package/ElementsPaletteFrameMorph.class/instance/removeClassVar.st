as yet unclassified
removeClassVar
	| var |
	var := self selectClassVar.
	var ifNotNil: [
		self currentClass removeClassVarName: var.
		self refresh ]