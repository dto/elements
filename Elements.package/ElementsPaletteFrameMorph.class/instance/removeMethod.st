as yet unclassified
removeMethod
	| sel |
	sel := self selectSelector.
	sel ifNotNil: [
		self currentClass removeSelector: sel.
		self refresh ]