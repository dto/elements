as yet unclassified
classVarMenu
	|menu|
	menu := MenuMorph new defaultTarget: self.
	menu add: 'add a class variable...' action: #createClassVar.
	menu add: 'remove a variable...' action: #removeClassVar.
	^menu