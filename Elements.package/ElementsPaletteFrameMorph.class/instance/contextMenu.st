as yet unclassified
contextMenu
	| choice |
	choice := header choice.
	(#basics = choice) ifTrue: [^nil ].

	(#(allClasses #currentPackageClasses) includes: choice) ifTrue: [
		^self classMenu].

	(#variables = choice) ifTrue: [
		(currentClass isMeta)
			ifTrue: [ ^self classVarMenu]
			ifFalse: [ ^self instanceVarMenu ]].

	(#instanceVars = choice) ifTrue: [^self instanceVarMenu].

	(#classVars = choice) ifTrue: [^self classVarMenu].

	(#globalVars = choice) ifTrue: [ ^self globalVarMenu].

	(#currentMessages = choice) ifTrue: [^self methodMenu].

	(#methodsClass = choice) ifTrue: [^nil ].