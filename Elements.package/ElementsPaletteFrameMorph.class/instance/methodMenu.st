as yet unclassified
methodMenu
	|menu|
	menu := MenuMorph new defaultTarget: self.
	menu add: 'remove a method...' action: #removeMethod.
	^menu