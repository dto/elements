as yet unclassified
removeInstVar
	| var |
	var := self selectInstVar.
	var ifNotNil: [
		self currentClass removeInstVarName: var.
		self refresh ]