as yet unclassified
globalVarMenu
	|menu|
	menu := MenuMorph new defaultTarget: self.
	menu add: 'add a global variable...' action: #createGlobalVar.
	menu add: 'remove a variable...' action: #removeGlobalVar.
	^menu