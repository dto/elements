as yet unclassified
buildPanes
	header := PaletteFrameHeadMorph new frame: self.
	palette := ElementsScrollFrameMorph new
				color: Color gray;
				borderColor: Color gray.
	self
		addMorph: header;
		addMorph: palette.