as yet unclassified
removeGlobalVar
	| var |
	var := self selectGlobalVar.
	var ifNotNil: [
		Smalltalk removeKey: var.
		self refresh ]