as yet unclassified
refresh
	| choice |
	choice := header choice.
	(#basics = choice) ifTrue: [
		palette contents: ElementPaletteMorph basics ].

	(#allClasses = choice) ifTrue: [
		palette contents: ElementPaletteMorph classes ].

	(#currentPackageClasses = choice) ifTrue: [
		(currentClass isKindOf: Metaclass)
			ifTrue: [ palette contents: (ElementPaletteMorph currentPackageClasses: currentClass allInstances first) ]
			ifFalse: [palette contents: (ElementPaletteMorph currentPackageClasses: currentClass) ]].


	(#variables = choice) ifTrue: [
		(currentClass isKindOf: Metaclass)
			ifTrue: [ palette contents: (ElementPaletteMorph classVarsFor: currentClass allInstances first) ]
			ifFalse: [palette contents: (ElementPaletteMorph instVarsFor: currentClass) ]].

	(#instanceVars = choice) ifTrue: [
		(currentClass isKindOf: Metaclass)
			ifTrue: [palette contents: (ElementPaletteMorph instVarsFor: currentClass allInstances first) ]
			ifFalse: [palette contents: (ElementPaletteMorph instVarsFor: currentClass) ]].

	(#classVars = choice) ifTrue: [
		(currentClass isKindOf: Metaclass)
			ifTrue: [palette contents: (ElementPaletteMorph classVarsFor: currentClass allInstances first) ]
			ifFalse: [palette contents: (ElementPaletteMorph classVarsFor: currentClass) ]].

	(#globalVars = choice) ifTrue: [
		palette contents: (ElementPaletteMorph globals) ].



	(#currentMessages = choice) ifTrue: [
		palette contents: (ElementPaletteMorph messagesFor: currentClass) ].

	(#methodsClass = choice) ifTrue: [
		palette contents: (ElementPaletteMorph messagesFor: header methodsClass) ].