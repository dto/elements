as yet unclassified
classMenu
	|menu|
	menu := MenuMorph new defaultTarget: self.
	menu add: 'create a subclass...' action: #createSubclass.
	menu add: 'remove a class...' action: #removeClass.
	^menu