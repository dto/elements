as yet unclassified
currentMethodsClass
	
	| name |
	name := palette contents submorphs last contents.
	(name endsWith: ' class') ifTrue: [
		name := (name reversed copyFrom: 6 to: name size) reversed.
		name := name reject: [:c| c = $ ].
		^(Smalltalk at: name asSymbol) class ].

	name := name reject: [:c| c = $ ].
	^Smalltalk at: name asSymbol