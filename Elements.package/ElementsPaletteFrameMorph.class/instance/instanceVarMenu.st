as yet unclassified
instanceVarMenu
	|menu|
	menu := MenuMorph new defaultTarget: self.
	menu add: 'add an instance variable...' action: #createInstVar.
	menu add: 'remove a variable...' action: #removeInstVar.
	^menu