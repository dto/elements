as yet unclassified
createInstVar
	|varName current|
	currentClass isMeta
		ifTrue: [current := currentClass theNonMetaClass ]
		ifFalse: [current := currentClass ].

	varName := FillInTheBlank request: 'variable name:' initialAnswer: ''.
	varName isEmpty ifTrue: [^self].
	varName := SyntaxElementMorph objectNameFrom: varName.
	current addInstVarName: varName.
	self refresh		