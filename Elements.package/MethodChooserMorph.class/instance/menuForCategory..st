as yet unclassified
menuForCategory: categoryName

	|menu list |
	menu := MenuMorph new
				defaultTarget: self.
	list := class organization listAtCategoryNamed: categoryName.
	list isEmpty ifTrue: [
		menu add: 'yourself' selector: #selector: argument: #yourself.
		^menu ].
	list asSortedCollection do: [:each|
		menu add: (self wordsFrom: each asString) selector: #selector: argument: each].
	^menu
