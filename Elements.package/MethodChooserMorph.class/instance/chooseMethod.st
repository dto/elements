as yet unclassified
chooseMethod

	|menu list|
	menu := MenuMorph new.
	menu defaultTarget: self.
	selector ifNotNil: [
		menu add: 'put method into category...' action: #reclassify.
		menu add: 'add category...' action: #addCategory.
		menu add: 'remove empty categories' action: #removeEmptyCategories.
		menu addLine ].
	list := class organization categories asSortedCollection.
	list do: [:each|
		menu add: each asString subMenu: (self menuForCategory: each)].
	menu popUpAt: button center x @ (self bottom + 5) forHand: World activeHand in: World