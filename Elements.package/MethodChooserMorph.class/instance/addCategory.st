as yet unclassified
addCategory
	| ans |
	ans := FillInTheBlank request: 'new category:' initialAnswer: ''.
	(ans size > 0) ifFalse: [^self].
	class organization addCategory: ans