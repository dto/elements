as yet unclassified
selector: aSelector

	selector := aSelector.
	label ifNil: [
		label := StringMorph contents: '' font: self labelFont.
		label color: self labelColor.
		self addMorphFront: label ].
	aSelector isNil
		ifTrue: [ label contents: '' ]
		ifFalse: [ label contents: (self wordsFrom: selector asString)].
	self fitMorphs.

	frame ifNotNil: [frame selectMethod: aSelector]
